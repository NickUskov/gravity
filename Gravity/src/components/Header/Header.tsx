import React, {FC, useEffect, useState} from 'react';
import { IconLock, IconBell } from "../Icons";
import styled from "styled-components";

interface HeaderProps {
    notifications?: boolean;
    onBellClick?: () => void;
    onLogOut?: () => void;
}

/* styled components */
const StyledHeader = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 24px;
  color: var(--white);
  font-size: 28px;
  
  .header-info {
    display: flex;
  }
  .icon-bell-wrapper {
    display: flex;
    align-items: center;
    cursor: pointer;
  }
`

const Header: FC<HeaderProps> = ({ onBellClick , onLogOut, notifications}) => {
    const [time, setTime] = useState<string>(`${new Date().getHours()}:${new Date().getMinutes()}`)
    useEffect(() => {
        const intervalId = setInterval(() => {
            setTime(`${new Date().getHours()}:${new Date().getMinutes()}`)
        }, 1000)

        return () => clearInterval(intervalId);
    })

    /* handlers */
    const handleLogOut = (): void => {
        onLogOut?.()
    }

    return (
        <StyledHeader>
            <span className="header-info">
                <span>{time}</span>
                <span>8 ноября</span>
                <span className="icon-bell-wrapper" onClick={(e) => {onBellClick?.()}}>
                    <IconBell filled={notifications}/>
                </span>

            </span>
            <span style={{ cursor: 'pointer' }} onClick={handleLogOut}>
                <IconLock />
            </span>
        </StyledHeader>
    );
};

export default Header;