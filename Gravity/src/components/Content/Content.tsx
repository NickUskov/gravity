import React, {FC, useState} from "react";
import styled from "styled-components";
import { Road } from "../Road";
import {Table} from "../Table";

interface ContentProps {}
export type ModeType = 'auto' | 'manual'


const ContentWrapper = styled.div`
  .mechanic-container {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 190px;
    padding-left: 200px;
    padding-right: 90px;
    .mechanic-block {
      display: flex;
      flex-direction: column;
      padding: 40px 80px;
      border-radius: 10px;
      background: #365DBE;
      span {
        text-align: left;
        line-height: 120%;
        color: #fff;
        letter-spacing: 5%;
        &:first-child {
          font-size: 36px;
          font-weight: 600;
        }
        &:last-child {
          margin-top: 30px;
          font-size: 30px;
        }
      }
    }
  }
  .content {
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    padding-bottom: 100px;
  }
  .table-container {
    width: 100%;
    height: 400px;
    padding-right: 30px;
    padding-left: 200px;
    margin-top: 30px;
    .table {
      width: 100%;
      height: 100%;
      padding: 20px 30px;
      background: #2f2f2f;
      border-radius: 15px;
    }
  }
  .progress-container {
    width: 100%;
    height: 45px;
    padding-right: 30px;
    padding-left: 200px;
    margin-top: 30px;
    .progress-bar {
      display: flex;
      width: 100%;
      height: 100%;
      justify-content: space-between;
      letter-spacing: 5%;
      span {
        font-size: 28px;
        color: #fff;
        margin-right: 30px;
      }
      .bar-field {
        height: 100%;
        background: #373737;
        display: flex;
        align-items: center;
        border-radius: 10px;
        color: #f2f2f2;
        font-weight: 300;
        padding: 0 20px;
        margin-right: 30px;
        &:last-child {
          margin-right: 0;
        }
        p {
          margin-left: 10px;
          margin-bottom: 0;
          color: #fff;
          font-weight: 500;
        }
      }
      .num {
        p {
          text-transform: uppercase;
        }
      }
      .photoelements {
        width: 59%;
      }
    }
  }
  
  @media (max-width: 1200px) {
    .table {
      padding: 20px!important;
    }
    
    .progress-bar {
      flex-direction: column;
      
      & > span {
        text-align: center;
        font-size: 22px!important;
      }
      
      .bar-field {
        display: flex;
        justify-content: center;
        width: 100%!important;
        margin-top: 8px;
        padding: 8px!important;
        font-size: 12px;
      }
    }
  }
`;

const dataSource = [
  {
    key: '1',
    ID: '12345',
    'Въезд': '24.03.2021 / 10:25',
    'Брутто': "12345",
    'Тара': "12345",
    'Нетто': "12345",
    'Категория': "ТКО-4",
    'Гос. номер': "А102ВВ102",
    'Комментарии': "Отсев",
  },
  {
    key: '2',
    ID: '12345',
    'Въезд': '24.03.2021 / 10:25',
    'Брутто': "12345",
    'Тара': "12345",
    'Нетто': "12345",
    'Категория': "ТКО-4",
    'Гос. номер': "А102ВВ102",
    'Комментарии': "Отсев",
  },
  {
    key: '3',
    ID: '12345',
    'Въезд': '24.03.2021 / 10:25',
    'Брутто': "12345",
    'Тара': "12345",
    'Нетто': "12345",
    'Категория': "ТКО-4",
    'Гос. номер': "А102ВВ102",
    'Комментарии': "Отсев",
  },

];

const columns = [
  {
    title: 'ID',
    dataIndex: 'ID',
    key: 'ID',
  },
  {
    title: 'Въезд',
    dataIndex: 'Въезд',
    key: 'Въезд',
  },
  {
    title: 'Брутто',
    dataIndex: 'Брутто',
    key: 'Брутто',
  },
  {
    title: 'Тара',
    dataIndex: 'Тара',
    key: 'Тара',
  },
  {
    title: 'Нетто',
    dataIndex: 'Нетто',
    key: 'Нетто',
  },
  {
    title: 'Категория',
    dataIndex: 'Категория',
    key: 'Категория',
  },
  {
    title: 'Гос. номер',
    dataIndex: 'Гос. номер',
    key: 'Гос. номер',
  },
  {
    title: 'Комментарии',
    dataIndex: 'Комментарии',
    key: 'Комментарии',
  },
];

const Content: FC<ContentProps> = (props) => {
  const [mode, setMode] = useState<ModeType>('auto')

  /* handlers */
  const handleModeChange = (modeType: ModeType) => {
    console.log(modeType, 'MODE CHANGED')
    setMode(modeType)
  }

  return (
      <ContentWrapper>
        {mode !== 'auto' ? (
          <div className="content">
            <div>
              <div className="mechanic-container">
                <div className="mechanic-block">
                  <span>Режим ручного управления шлагбаумами</span>
                  <span>
                    Внимание! В этом режиме контроль взвешивания не происходит и не идет
                    к учету! Здесь Вы можете открывать шлагубаумы вручную. Для того
                    чтобы перейти в автоматический режим нажмите на кнопку “Автомат”.
                  </span>
                </div>
              </div>
            </div>
            <Road onModeChange={handleModeChange} />
          </div>) : (
          <div className="content">
            <div>
              <div className="table-container">
                <div className="table">
                  <Table
                      stripped={false}
                      tableHeader="Текущие события"
                      dataSource={dataSource}
                      columns={columns}
                      pagination={false}
                  />
                </div>
              </div>
              <div className="progress-container">
                <div className="progress-bar">
                  <span>Процесс заезда</span>
                  <div className="bar-field num">
                    Гос. номер: <p>a102bb102</p>
                  </div>
                  <div className="bar-field photoelements">
                    Прогресс: <p>Ожидание пересечения фотоэлементов</p>
                  </div>
                  <div className="bar-field timer">
                    Таймер: <p>210 с</p>
                  </div>
                </div>
              </div>
            </div>
            <Road onModeChange={handleModeChange} />
          </div>
      )}
    </ContentWrapper>
  );
};

export default Content;
