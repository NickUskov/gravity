import React, { FC } from 'react';
import { Select } from "../Select";
import { Input } from "../Input";
import { IconSearch } from "../Icons";
import { Table } from '../Table';

import styled from "styled-components";

interface StatsProps {}

const StatsWrapper = styled.div`
  flex: 1;
  height: 100%;
  padding: 20px 20px 20px 0;
  color: white;
  
  .stats-content {
    height: inherit;
    padding: 20px 30px;
    margin-left: 200px;
    border-radius: 14px;
    background: #2F2F2F;
    
    &-header {
      margin: 0;
      margin-bottom: 42px;
      text-align: center;
      font-size: 28px;
      line-height: normal;
    }
    
    &-filter {
      display: flex;
      margin-bottom: 32px;
    }
    
    &-filter-item {
      margin-right: 20px;
    }

    &-filter-table {
      max-height: 600px;
    }
  }
  
  @media (max-width: 1200px) {
    .stats-content {
      padding: 20px!important;
    }
    
    .stats-content-header {
      margin-bottom: 32px;
      font-size: 22px;
    }
  }
`

const dataSource = [
    {
        key: '1',
        ID: '12345',
        'Гос. номер': 'А102ВВ102',
        'Перевозчик': 'ООО”Альянс”',
        'Брутто': '12345',
        'Тара': '12345',
        'Нетто': '12345',
        'Категория': 'ТКО-4',
        'Вид груза': 'Картон',
        'Дата въезда': '24.03.2021 / 10:25',
        'Дата выезда': '24.03.2021 / 10:25',
        'Комментарии': 'Отсев',
    },
    {
        key: '2',
        ID: '12345',
        'Гос. номер': 'А102ВВ102',
        'Перевозчик': 'ООО”Альянс”',
        'Брутто': '12345',
        'Тара': '12345',
        'Нетто': '12345',
        'Категория': 'ТКО-4',
        'Вид груза': 'Картон',
        'Дата въезда': '24.03.2021 / 10:25',
        'Дата выезда': '24.03.2021 / 10:25',
        'Комментарии': 'Отсев',
    },
    {
        key: '3',
        ID: '12345',
        'Гос. номер': 'А102ВВ102',
        'Перевозчик': 'ООО”Альянс”',
        'Брутто': '12345',
        'Тара': '12345',
        'Нетто': '12345',
        'Категория': 'ТКО-4',
        'Вид груза': 'Картон',
        'Дата въезда': '24.03.2021 / 10:25',
        'Дата выезда': '24.03.2021 / 10:25',
        'Комментарии': 'Отсев',
    },

];

const columns = [
    {
        title: 'ID',
        dataIndex: 'ID',
        key: 'ID',
    },
    {
        title: 'Гос. номер',
        dataIndex: 'Гос. номер',
        key: 'Гос. номер',
    },
    {
        title: 'Перевозчик',
        dataIndex: 'Перевозчик',
        key: 'Перевозчик',
    },
    {
        title: 'Брутто',
        dataIndex: 'Брутто',
        key: 'Брутто',
    },
    {
        title: 'Тара',
        dataIndex: 'Тара',
        key: 'Тара',
    },
    {
        title: 'Нетто',
        dataIndex: 'Нетто',
        key: 'Нетто',
    },
    {
        title: 'Категория',
        dataIndex: 'Категория',
        key: 'Категория',
    },
    {
        title: 'Вид груза',
        dataIndex: 'Вид груза',
        key: 'Вид груза',
    },
    {
        title: 'Дата въезда',
        dataIndex: 'Дата въезда',
        key: 'Дата въезда',
    },
    {
        title: 'Дата выезда',
        dataIndex: 'Дата выезда',
        key: 'Дата выезда',
    },
    {
        title: 'Комментарии',
        dataIndex: 'Комментарии',
        key: 'Комментарии',
    },
];

const Stats: FC<StatsProps> = () => {
    return (
        <StatsWrapper>
            <div className="stats-content">
                <p className="stats-content-header">Статистика</p>
                <div className="stats-content-filter">
                    <Select
                        className='stats-content-filter-item'
                        style={{width: 200, height: 40}}
                        placeholder="Категория"
                    />
                    <Select
                        className='stats-content-filter-item'
                        style={{width: 200, height: 40}}
                        placeholder="Вид груза"
                    />
                    {/*<Input*/}
                    {/*    placeholder="Гос.номер"*/}
                    {/*    suffix={<IconSearch />}*/}
                    {/*/>*/}
                </div>

                <div className="stats-content-table">
                    <Table
                        stripped
                        dataSource={dataSource}
                        columns={columns}
                        pagination={false}
                    />
                </div>
            </div>
        </StatsWrapper>
    );
};

export default Stats;