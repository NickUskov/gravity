import React, { FC } from 'react';
import { Input as AntdInput, InputProps as AntdInputProps} from 'antd'
import styled from "styled-components";

interface InputProps extends AntdInputProps{}

/* styled components */
const InputWrapper = styled.div`
    .ant-input {
      height: 42px;
      border: 1px solid var(--inputColor);
      border-radius: 5px;
      color: var(--white);
      background: transparent;
      box-shadow: none!important;
      
      &:hover {
        border: 1px solid var(--white);
      }
      
      input {
        background: inherit;
        color: inherit;
        
        &::placeholder {
          color: var(--inputColor);
        }
      }
    }
`

const Input: FC<InputProps> = (props) => {
    return (
        <InputWrapper>
            <AntdInput {...props} />
        </InputWrapper>
    );
};

export default Input;