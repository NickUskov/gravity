import React, {FC, useRef} from 'react';
import { Modal as AntModal, ModalProps as AntModalProps } from 'antd';
import styled from "styled-components";
import {Select} from "../Select";
import {Input} from "../Input";

interface BruttoModalProps extends AntModalProps {};

const BruttoModalWrapper = styled.div`
  .ant-modal-mask {
    backdrop-filter: blur(2px);
  }

  .brutto-modal {

    .ant-modal-content {
      width: 690px;
      border-radius: var(--modalBorderRadius);
      background: var(--modalBg)!important;
      box-shadow: none;

      .ant-modal-body {
        padding: 30px 40px 40px 40px;
        font-size: 28px;
        text-align: center;
        color: var(--white);
        
        p {
          margin-bottom: 26px;
          line-height: normal;
        }
      }

      .ant-modal-footer {
        padding: 0 40px 40px 40px;
        border: 0;
      }
    }

    .ant-modal-close-x {
      display: none;
    }
  }
`

const BruttoModal: FC<BruttoModalProps> = (props) => {
    const wrapperRef = useRef<any>(null)
    return (
        <BruttoModalWrapper ref={wrapperRef}>
            <AntModal
                {...props}
                wrapClassName="brutto-modal"
                maskClosable={false}
                getContainer={wrapperRef?.current}
                centered
            >
                <p>Взвешивание брутто</p>
                <Input style={{ marginBottom: 6 }} placeholder="Комментарий" />
                <Select style={{ marginBottom: 6 }} placeholder="Контрагент" />
                <Select style={{ marginBottom: 6 }} placeholder="Площадка" />
                <Select style={{ marginBottom: 6 }} placeholder="Категория" />
                <Select style={{ marginBottom: 6 }} placeholder="Вид груза" />
                <Input style={{ marginBottom: 6 }} placeholder="Комментарий" />
            </AntModal>
        </BruttoModalWrapper>
    );
};

export default BruttoModal;