import React, { FC } from 'react';
import { Button as AntdButton, ButtonProps as AntdButtonProps } from 'antd'
import styled from "styled-components";

interface MainButtonProps extends AntdButtonProps {}

const BtnWrapper = styled.div`
    .ant-btn {
      border: 1px solid var(--white);
      border-radius: var(--buttonBorderRadius);
      font-size: 22px;
      color: var(--white);
      background: var(--buttonBg);
      
      &:hover {
        border: 1px solid var(--accentColor);
        background: var(--accentColor);
      }
    }

  .ant-btn-primary {
    border: 1px solid var(--buttonAccentColor);
    letter-spacing: 1px;
    color: var(--buttonAccentColor);
    background: var(--buttonBg);
    
    &:hover {
      color: white;
      background: var(--accentColor);
    }
  }
`

const MainButton: FC<MainButtonProps> = (props) => {
    return (
        <BtnWrapper>
            <AntdButton {...props}/>
        </BtnWrapper>
    );
};

export default MainButton;